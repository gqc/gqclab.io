---
title: Miembros
subtitle: que son/fueron del grupo
comments: false
translationKey: people
weight: 3
---

### Investigador principal
- Reinaldo Pis Diez 

### Investigadores
- Carlos A. Franca
- Martín J. Lavecchia
- Hernán R. Sanchez

### Postdocs
- Julián Del Pla
- Leonardo E. Riafrecha

### Doctorales
- Leandro Bof
- Roberto Rousse
- Vladimir Coussirat
- Patricia Quispe (Pato)

### Alumnos
- Leandro Martinez Heredia
- Diego A. Pascua

### Antiguos miembros
- Santiago Miranda
- Santiago Cingolani
- Verónica Ferraresi Curotto
- Samanta M. Carrión
- María Luján Alegre

