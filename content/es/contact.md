---
layout: page
title: Contacto
subtitle: Estamos acá
weight: 1
---

### Dirección
**CEQUINOR** Centro de Química Inorgánica CONICET-UNLP
Boulevard 120 e/ 60 y 64 Nº1465, La Plata – Buenos Aires – Argentina

### Teléfono
(+54) 0221 445-4393
