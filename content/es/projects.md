---
layout: page
title: Proyectos
#subtitle: a summary
weight: 4
---

El GQC desarrolla dos áreas principales de investigación:

## Materiales
- Pequeños *clusters* de metales de transición.
- Adsorción de moléculas pequeñas en superficies atómicas.


## Sistemas biológicos
- Predicción de propiedades estructurales y espectroscópicas.
- *Virtual screening* estructural.
- Energías de unión ligando/receptor
- **NaturAr** - Una base de datos de productos naturales de Argentina [naturar.quimica.unlp.edu.ar](https://naturar.quimica.unlp.edu.ar).

