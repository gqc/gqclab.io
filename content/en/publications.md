---
layout: page
title: Publications
weight: 20
---

- [On the discovery of a potential survivin inhibitor combining computational tools and cytotoxicity studies](https://doi.org/10.1016/j.heliyon.2019.e02238)
Patricia A. Quispe, Martín J. Lavecchia, Ignacio E. León.
Heliyon
**2019**

- [A retention index-based QSPR model for the quality control of rice](https://doi.org/10.1016/j.jcs.2017.11.004)
Cristian Rojas, Piercosimo Tripaldi, Andrés Pérez-González, Pablo Duchowicz, Reinaldo Pis-Diez.
Journal of Cereal Science
**2018**

- [A novel oxidovanadium (V) compound with an isonicotinohydrazide ligand. A combined experimental and theoretical study and cytotoxity against K562 cells](https://doi.org/10.1016/j.poly.2017.07.013)
Ana C González-Baró; Verónica Ferraresi-Curotto, Reinaldo Pis-Diez, Beatriz S Parajón Costa, Jackson ALC Resende, Flávia CS de Paula, Elene C Pereira-Maia, Nicolás A Rey.
Polyhedron
**2017**

- [Bidimensional Spectroelectrochemistry: application of a new device in the study of a o-vanillin-copper (II) complex](https://doi.org/10.1016/j.electacta.2017.05.105)
D Izquierdo, V Ferraresi-Curotto, A Heras, R Pis-Diez, AC Gonzalez-Baro, A Colina.
Electrochimica Acta
**2017**


