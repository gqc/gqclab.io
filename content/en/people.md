---
title: People
subtitle: who work(ed) in GQC
comments: false
translationKey: people
weight: 30
---

### Principal Researcher
- Reinaldo Pis Diez 

### Researchers
- Carlos A. Franca
- Martín J. Lavecchia

### Postdocs
- Julián Del Pla
- Leonardo E. Riafrecha

### PhD students
- Leandro Bof
- Roberto Rousse
- Patricia Quispe (Pato)
- Julián Fernández
- Leandro Martinez Heredia

### Alumni

### Colaborators
- Diego A. Pascua
- Vladimir Coussirat

### Former Members
- Hernán R. Sanchez
- Santiago Miranda
- Santiago Cingolani
- Verónica Ferraresi Curotto
- Samanta M. Carrión
- María Luján Alegre

