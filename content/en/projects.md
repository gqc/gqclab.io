---
layout: page
title: Projects
subtitle: a summary
weight: 40
---

The GQC carries out two main areas of work:


## Materials
- Small cluster of transition metals.
- Adsorption of small molecules on atomic surfaces.


## Biological systems
- Prediction of structural and spectroscopic properties.
- Structural virtual screening.
- Ligand/Receptor binding energies.
- NaturAr - A Database of Natural Compounds from Argentina [naturar.quimica.unlp.edu.ar](https://naturar.quimica.unlp.edu.ar).

