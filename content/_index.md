## GQC/QuimComm
Página del Grupo de Química Computacional del CEQUINOR, GQC o QuimComm para los copados. \\
O LQT para los del centro (?


<div class="posts-list">
  {{ $pag := .Paginate (where .Data.Pages "Type" "post") }}
  {{ range .Pages }}
    {{ .Render "Summary" }}
    <article class="post-preview">
      <a href="{{ .Permalink }}">
	<h2 class="post-title">{{ .Title }}</h2>
	{{ if .Params.subtitle }}
	  <h3 class="post-subtitle">
	  {{ .Params.subtitle }}
	  </h3>
	{{ end }}
      </a>

      {{ partial "post_meta.html" . }}
      <div class="post-entry">
	{{ if .Truncated }}
	  {{ .Summary }}
	  <a href="{{ .Permalink }}" class="post-read-more">[{{ i18n "readMore" }}]</a>
	{{ else }}
	  {{ .Content }}
	{{ end }}
      </div>

      {{ if .Params.tags }}
	<span class="post-meta">
	{{ range .Params.tags }}
	  #<a href="{{ $.Site.LanguagePrefix }}/tags/{{ . | urlize }}">{{ . }}</a>&nbsp;
	{{ end }}
	</span>
      {{ end }}
    </article>
  {{ end }}
</div>
